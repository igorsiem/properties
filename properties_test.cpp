/*
 * This file is part of the Properties library, written by Igor Siemienowicz
 * and maintained at https://bitbucket.org/igorsiem/properties
 *
 * Copyright (c) 2016 Igor Siemienowicz All rights reserved
 * Distributed under the Boost Software License, Version 1.0. (See
 * accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
 
#include <string>

#define CATCH_CONFIG_MAIN
#include <CATCH/catch.hpp>

#include <properties/properties.h>
using namespace properties;

TEST_CASE("basic readable properties can retrieve values",
		"[read][reference][value][int][string]")
{
	// Basic retrieval of a value
	auto ipv = ReadableValueProperty<int>([](void) { return 1; });
	REQUIRE(ipv.get() == 1);

	// A property that retrieves a reference to data.
	auto i = 1;
	auto ipr = ReadableReferenceProperty<int>([&i](void)->const int&
		{ return i; });
	REQUIRE(ipr.get() == 1);

	// If the data changes, so does the property.
	i = 2;
	REQUIRE(ipr.get() == 2);

	// Can retrieve using the function-call operator
	REQUIRE(ipv() == 1);
	REQUIRE(ipr() == 2);

	// Can retrieve values directly using a cast.
	int v = ipv, r = ipr;
	REQUIRE(v == 1);
	REQUIRE(r == 2);

	// Can compare with value types
	REQUIRE(ipv == 1);
	REQUIRE(ipr == 2);
	REQUIRE(ipr != 3);
	REQUIRE(ipr < 3);

	// Can compare properties with properties
	REQUIRE(ipv != ipr);

	// Reference tests with a string.
	std::string s = "abc";
	auto sr = ReadableReferenceProperty<std::string>(
		[&s](void)->const std::string&{ return s; });

	REQUIRE(sr == "abc");	// basic retrieval
	s = "xyz";
	REQUIRE(sr == "xyz");	// retrieval of updated value
	REQUIRE(sr != "abc");	// comparison...
	REQUIRE(sr > "abc");	// ... tests
}	// end basic readable properties test

TEST_CASE("basic writeable properties can set values",
		"[write][int][string]")
{
	auto i = 1;
	auto s = std::string("abc");

	auto iw = WriteableProperty<int>([&i](const int& v) { i = v; });
	auto sw = WriteableProperty<std::string>([&s](const std::string& v)
		{ s = v; });

	iw.set(2);			// set using setter
	REQUIRE(i == 2);
	iw = 3;				// set using assignment
	REQUIRE(i == 3);

	sw.set("xyz");			// set using setter
	REQUIRE(s == "xyz");
	sw = "lmnop";			// set using assignment
	REQUIRE(s == "lmnop");
}	// end writeable property test

TEST_CASE("readabl/writeable properties can retrieve and set values",
		"[read][reference][value][write][int][string]")
{
	using RWIntRefPrp = ReadReferenceWriteProperty<int>;
	using RWIntValPrp = ReadValueWriteProperty<int>;
	using RWStrRefPrp = ReadReferenceWriteProperty<std::string>;

	auto a = 0, b = 1;
	auto s = std::string("abc");

	// A property with reference readability for 'a'
	auto a_prp = RWIntRefPrp(
		[&a](void) -> const int& { return a; },	// getter
		[&a](const int& v) { a = v; });			// setter

	// A property with value readability for 'b'
	auto b_prp = RWIntValPrp(
		[&b](void) { return b; },
		[&b](const int& v) { b = v; });

	// A string property (with reference readability) for 'a'
	auto s_prp = RWStrRefPrp(
		[&s](void) -> const std::string&{ return s; },
		[&s](const std::string& v) { s = v; });

	// Basic read-value check.
	REQUIRE(a_prp.get() == 0);
	REQUIRE(a_prp() == 0);
	REQUIRE(a_prp == 0);
	REQUIRE(b_prp.get() == 1);
	REQUIRE(b_prp() == 1);
	REQUIRE(b_prp == 1);
	REQUIRE(s_prp.get() == "abc");
	REQUIRE(s_prp() == "abc");
	REQUIRE(s_prp == "abc");

	// Check assignment syntax; note that result is checked using underlying
	// data.
	a_prp.set(2);
	REQUIRE(a == 2);
	
	a_prp = 3;
	REQUIRE(a == 3);

	b_prp.set(4);
	REQUIRE(b == 4);

	b_prp = 5;
	REQUIRE(b == 5);

	s_prp.set("xyz");
	REQUIRE(s == "xyz");

	s_prp = "lmnop";
	REQUIRE(s == "lmnop");

	// Check comparisons.
	REQUIRE(a_prp != 2);
	REQUIRE(a_prp > 2);
	REQUIRE(a_prp != b_prp);
	REQUIRE(b_prp > a_prp);
	REQUIRE(s_prp != "abc");
	REQUIRE(s_prp > "abc");

}	//end read/write property test

TEST_CASE("setters can be used to validate updates",
		"[write][validate][int]")
{
	// Set up a writeable property with a setter that 'clips' values to the
	// range [1,5].
	auto i = 1;
	auto i_prp = WriteableProperty<int>(
		[&i](const int& v)
		{
			if (v < 1) i = 1;
			else if (v > 5) i = 5;
			else i = v;
		});

	REQUIRE(i == 1);
	i_prp = 0;
	REQUIRE(i == 1);	// value clipped to 1
	i_prp = 3;
	REQUIRE(i == 3);	// value in range
	i_prp = 7;
	REQUIRE(i == 5);	// value clipped to 5

	// Can set up a writeable property that throws an exception if the set
	// value is out of range.
	auto i2_prp = WriteableProperty<int>(
		[&i](const int& v)
		{
			if ((v < 1) || (v > 5))
				throw std::runtime_error("value out of range");

			i = v;
		});

	// Legit value is in range.
	REQUIRE_NOTHROW(i2_prp = 2);
	REQUIRE(i == 2);

	// Out of range value throws an exception
	REQUIRE_THROWS_AS(i2_prp = 7, std::runtime_error);
}	// end setter validation test

TEST_CASE("setters can implement a observer pattern for changes",
		"[write][observe][int]")
{
	auto change_observed = false;	// simple Observer flag
	auto i = 5;
	auto i_prp = WriteableProperty<int>(
		[&i, &change_observed](const int& v)
		{
			i = v;
			change_observed = true;
		});

	// Make a change - flag is raised.
	i_prp = 3;
	REQUIRE(i == 3);
	REQUIRE(change_observed);
}	// end observer test

TEST_CASE("lazy-calculated properties can be constructed from readable "
		"properties", "[read][value][reference][lazy][calculate][int]")
{
	// Set up two ordinary readable properties - one is reference the other
	// value, just for generality.
	auto a = 1, b = 2;
	auto a_prp = ReadableReferenceProperty<int>(
		[&a](void)->const int& { return a; });
	auto b_prp = ReadableValueProperty<int>([&b](void)->int { return b; });

	// Create a readable value property that derives its value by adding the
	// values of a_prp and b_prp. This is a 'lazy-calculated' property,
	// because it performs its calculation only on demand - at the last
	// possible moment.
	//
	// Lazy-calculated properties are optimal when the input values change
	// more reqently than the calculated is performed.
	auto calc_prp = ReadableValueProperty<int>([&a_prp, &b_prp](void)->int
	{
		return a_prp + b_prp;
	});

	REQUIRE(calc_prp == a + b);

	// We can update a and b and the calculated value will change
	// accordingly.
	a = 5;
	b = 10;
	REQUIRE(calc_prp == a+b);
}	// end lazy-calculated properties test

TEST_CASE("eager-calculated properties can be constructed from observable "
		"read/write properties", "[read][write][reference][value][int]"
		"[eager][calculate][observe]")
{
	auto a = 1, b = 2;

	// A simple 'notifier' function / lambda type.
	using notifier = std::function<void(void)>;
	auto a_updated_fn = notifier(nullptr), b_updated_fn = notifier(nullptr);

	// These read/write properties call the notifier functions above (if they
	// are not nullptr) when they are updated.
	auto a_prp = ReadReferenceWriteProperty<int>(
		[&a](void)->const int& { return a; },		// standard ref getter
		[&a,&a_updated_fn](const int& v)			// setter that notifies
		{
			a = v;
			if (a_updated_fn != nullptr) a_updated_fn();
		});

	auto b_prp = ReadValueWriteProperty<int>(
		[&b](void)->int { return b; },				// standard val getter
		[&b, &b_updated_fn](const int& v)			// setter that notifies
		{
			b = v;
			if (b_updated_fn != nullptr) b_updated_fn();
		});

	// We now construct an 'eager calculated' property. This simply uses the
	// notifier function objects above to update a stored value, around which
	// we have an ordinary readable property.
	//
	// Eager-calculated properties are optimal when the calculated value will
	// be read more often than the input values are updated.
	auto c = a + b;

	// This is a calculation function - the notifier variables will be set
	// to this so that c is recalculated whenever a or b change.
	auto c_calc = [&a, &b, &c](void) { c = a + b; };
	a_updated_fn = c_calc;
	b_updated_fn = c_calc;

	// We wrap an ordinary readable reference property around c - we could
	// just as easily access it directly, but this reinforces the idea that
	// c is meant to be read-only (since it is calculated).
	auto c_prp =
		ReadableReferenceProperty<int>([&c](void)->const int& { return c; });

	REQUIRE(c_prp == a + b);

	// Once again, when a or b change, c changes too.
	a_prp = 5;
	REQUIRE(c_prp == a + b);
	b_prp = 20;
	REQUIRE(c_prp == a + b);
}	// end eager-derived properties test

TEST_CASE("properties can be used in a multi-threaded environment",
		"[read][reference][write][int][string][thread]")
{
	// Set up a couple of data items, each with their own mutexes. Note that
	// basic Properties (like ReadWriteProperty) do not directly
	// thread-protect their data, but we can still use the primitives defined
	// in threadsafety.h easily enough.
	//
	// This is intended more as a demonstration of how to implement thread-
	// safety with primitive properties, than a test of the library
	// capabilities per se.
	//
	// This test is reimplemented (more simply) using ContainerProperties,
	// which have built-in multi-threading.
	using ts = StandardThreadSafety;
	auto i = 5;
	ts::shared_mutex i_mtx;
	auto s = std::string("abc");
	ts::shared_mutex s_mtx;

	// Now set up read/write properties with accessors that use the
	// mutexes to protect the data.
	auto i_prp = ReadReferenceWriteProperty<int>(
		[&i, &i_mtx](void)->const int&
		{
			ts::shared_lock l(i_mtx);	// getter uses a shared lock
			return i;
		},
		[&i, &i_mtx](const int& v)
		{
			ts::unique_lock l(i_mtx);	// setter uses a unique lock
			i = v;
		});

	auto s_prp = ReadReferenceWriteProperty<std::string>(
		[&s, &s_mtx](void)->const std::string&
		{
			ts::shared_lock l(s_mtx);
			return s;
		},
		[&s, &s_mtx](const std::string& v)
		{
			ts::unique_lock l(s_mtx);
			s = v;
		});

	// First, check the values the ordinary way.
	REQUIRE(i_prp == 5);
	REQUIRE(s_prp == "abc");

	// This lambda increments the integer property from 0 to 20.
	auto incrementer = [&i_prp](void)
	{
		i_prp = 0;
		while (i_prp < 20)
		{
			i_prp = (i_prp + 1);	// Note - could implement increment
									// operator for integer properties.

			// Sleep for a bit to let other threads work.
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
							
	};	// end incrementer

	// This lambda convert the integer property to a string
	auto converter = [&i_prp, &s_prp](void)
	{
		while (i_prp < 20)
		{
			std::stringstream strm1;
			strm1 << i_prp;
			s_prp = strm1.str();
			std::this_thread::sleep_for(std::chrono::milliseconds(11));
		}

		std::stringstream strm2;
		strm2 << i_prp;
		s_prp = strm2.str();
	};

	// Execute our lambdas in two separate threads.
	std::thread t1(incrementer), t2(converter);
	t1.join();
	t2.join();

	REQUIRE(i_prp == 20);
	REQUIRE(s_prp == "20");
}	// end multi-threading test

TEST_CASE("container properties support full validation and observer "
		"functionality", "[read][write][reference][int][container][validate]"
		"[observe]")
{
	// Set up a property that will only accept positive integers, and
	// notifies of successful updates.
	bool updated = false;
	auto validator = [](const int& ov, const int& nv) {
		if (nv < 0) return false; return true; };
	auto notifier = [&updated](void) { updated = true; };
	auto i_prp = ContainerProperty<int>(
		1,	// default value
		{ validator },
		{ notifier });

	// Basic value check
	REQUIRE(i_prp == 1);

	// Attempt to set to -1 - this should fail, and notifier will not be
	// called.
	i_prp = -1;
	REQUIRE(i_prp == 1);
	REQUIRE_FALSE(updated);

	// Attempt to set to 5 - this will succeed.
	i_prp = 5;
	REQUIRE(i_prp == 5);
	REQUIRE(updated);
}	// end container validation and notification test

TEST_CASE("container properties have built-in multi-threading support",
		"[read][write][reference][container][thread][int][string]")
{
	// This is a very similar test to the one entitled "properties can be
	// used in a multi-threaded environment", but implemented using
	// ContainerProperties.
	//
	// Two properties - int and string.
	using ts = StandardThreadSafety;
	auto i_prp = ContainerProperty<int, ts>(5);
	auto s_prp = ContainerProperty<std::string, ts>("abc");

	// First, check the values the ordinary way.
	REQUIRE(i_prp == 5);
	REQUIRE(s_prp == "abc");

	// This lambda increments the integer property from 0 to 20.
	auto incrementer = [&i_prp](void)
	{
		i_prp = 0;
		while (i_prp < 20)
		{
			i_prp = (i_prp + 1);	// Note - could implement increment
									// operator for integer properties.

									// Sleep for a bit to let other threads work.
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}

	};	// end incrementer

	// This lambda convert the integer property to a string
	auto converter = [&i_prp, &s_prp](void)
	{
		while (i_prp < 20)
		{
			std::stringstream strm1;
			strm1 << i_prp;
			s_prp = strm1.str();
			std::this_thread::sleep_for(std::chrono::milliseconds(11));
		}

		std::stringstream strm2;
		strm2 << i_prp;
		s_prp = strm2.str();
	};	// end converter

		// Execute our lambdas in two separate threads.
	std::thread t1(incrementer), t2(converter);
	t1.join();
	t2.join();

	REQUIRE(i_prp == 20);
	REQUIRE(s_prp == "20");

}	// end container thread test

// This class encapsulates a person's name, with all its parts, and a
// 'composition' function that puts them all together. It is intended to
// demonstrate a number of basic Properties library principles, including
// eager-calculated properties and thread-safety.
class TestName
{
public:

	using ts = StandardThreadSafety;
	using mutex = ts::shared_mutex;
	using rdlck = ts::shared_lock;
	using wrtlck = ts::unique_lock;
	using NameComponentProperty = ContainerProperty<std::string, ts>;

	NameComponentProperty title;
	NameComponentProperty first;
	NameComponentProperty middle;
	NameComponentProperty last;
	NameComponentProperty suffix;
	ReadableReferenceProperty<std::string> full;	// eager-calculated
													// property

	// Constructor
	//
	// Note that all of the name components are constructed with a notifier
	// function that calls 'derive_full'.
	TestName(
			const std::string& ttl,
			const std::string& frst,
			const std::string& mdl,
			const std::string& lst,
			const std::string& sfx) :
		title(ttl),
		first(frst),
		middle(mdl),
		last(lst),
		suffix(sfx),
		full([this](void)->const std::string&{
			rdlck l(m_full_mutex); return m_full; }),
		m_full_mutex(),
		m_full()
	{
		// Add a notifier to each of the name component properties, so that
		// when they change, the full name will be recalculated. Thes could
		// have been set up in the constructor list itself, but that looks
		//ugly(er).
		auto notifier = [this](void) { derive_full(); };
		title.add_notifier(notifier);
		first.add_notifier(notifier);
		middle.add_notifier(notifier);
		last.add_notifier(notifier);
		suffix.add_notifier(notifier);

		// Now, derive the full name initially
		derive_full(); 

	}	// end constructor

	// Derive the full name from the name components
	void derive_full(void)
	{
		std::stringstream nm;
		nm << title();			// could implement << operator string props

		if (nm.str().size() && nm.str().back() != ' ' && first().size())
			nm << " ";
		nm << first();

		if (nm.str().size() && nm.str().back() != ' ' && middle().size())
			nm << " ";
		nm << middle();

		if (nm.str().size() && nm.str().back() != ' ' && last().size())
			nm << " ";
		nm << last();

		if (nm.str().size() && nm.str().back() != ' ' && suffix().size())
			nm << " ";
		nm << suffix();

		wrtlck l(m_full_mutex);
		m_full = nm.str();
	}

protected:

	// Mutex protecting the calculated full name
	mutable mutex m_full_mutex;

	// Storage for the calculated full name
	std::string m_full;
};	// end TestName class

TEST_CASE("properties may be used as attributes of classes"
		"[read][write][reference][container][thread][string][class]")
{
	
	// Do some basic checks.
	TestName joe("Mr", "Joseph", "Harvey", "Bloggs", "Sr");
	REQUIRE(joe.full == "Mr Joseph Harvey Bloggs Sr");

	joe.middle = "";
	REQUIRE(joe.full == "Mr Joseph Bloggs Sr");

	joe.title = "";
	joe.first = "Joe";
	joe.suffix = "";
	REQUIRE(joe.full == "Joe Bloggs");

}	// end properties in classes
