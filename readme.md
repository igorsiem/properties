Properties
==========
*Properties* is a small, header-only template library for implementing a
variety of property-like idioms in C++. Currently, this project is very much
a work in progress.

> *TODO: Where to get the latest source*

> *TODO: Where to find documentation*

Copyright and License
---------------------
This file is part of the *Properties* project, by Igor Siemienowicz. Unless
otherwise noted, all code and other material included with this project is
copyright (c) 2016 Igor Siemienowicz. All rights reserved.

Distributed under the Boost Software License, Version 1.0. (See accompanying
file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt).
