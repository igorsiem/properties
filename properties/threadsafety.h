/*
* This file is part of the Properties library, written by Igor Siemienowicz
* and maintained at https://bitbucket.org/igorsiem/properties
*/

/**
* \file threadsafety.h Thread-safety primitives
*/

#include <shared_mutex>

#ifndef _PROPERTIES_threadsafety_h_included
#define _PROPERTIES_threadsafety_h_included

/**
 * \page thread-safety Notes on Thread-safety
 *
 * This library includes some basic functionality to support the use of
 * Properties in a multi-threaded environment. For most of the primitive
 * Property classes (ReadWriteProperty and its ancestors), thread-safety is
 * entirely the responsibility of the getters and setters that are used to
 * construct the Property object.
 *
 * However, classes that implement their own internal thread-safety (such as
 * ContainerProperty) use the following basic constructs to optionally
 * support multi-threaded environments.
 *
 * For the sake of simplicity, and in order to allow different threading
 * models and libraries to be used, the *Properties* library uses a simple
 * struct type argument that defines three essential sub-types:
 *
 * -   `shared_mutex` --- Any type similar to `std::shared_mutex`
 *
 * -   `shared_lock` --- A type similar to `std::shared_lock`, and
 *     specifically having a constructor that takes a reference to
 *	   `shared_mutex`
 *
 * -   `unique_lock` --- A type similar to `std::unique_lock`, and
 *	   specifically having a constructor that takes a reference to
 *     `shared_mutex`
 *
 * Note that the above sub-types and requirements are intentionally very
 * loose, to allow a variety of existing threading libraries to be used with
 * *Properties*.
 *
 * *Properties* supplies two implementations of this:
 *
 * -   `NoThreadSafety` --- Three empty structs that can be instantiated and
 *     used as if they were thread-safety primitives, but which have no
 *     effect; when these are used, the result will be that there is no
 *     thread-safety, and there should be no overhead as a result; this is
 *     the default setting of the thread-safety type parameter for the
 *     Property templates
 *
 * -   `StandardThreadSafety` --- A set of aliases for the standard C++
 *     thread-safety primitives
 *
 * Other thread-safety primitives may be used by specifying a struct that
 * contains definitions that follow this pattern. One example might be
 * Windows critical sections (with suitable wrappers).
 */

namespace properties
{
	/**
	 * \brief A template for definitions of thread-safety primitives in this
	 * library
	 */
	template<
		typename SharedMutexType,
		typename SharedLockType,
		typename UniqueLockType>
	struct ThreadSafetyDefinition
	{
		using shared_mutex = SharedMutexType;
		using shared_lock = SharedLockType;
		using unique_lock = UniqueLockType;
	};	// end ThreadSafetyDefinition class template

	/**
	 * \brief A empty struct for use as a non-functional getter_mutex type
	 */
	struct NullMutex {};

	/**
	 * \brief An emptry struct for use as a non-functional lock type
	 */
	struct NullLock
	{
		/**
		 * \brief Trivial constructor taking a NullMutex
		 */
		NullLock(NullMutex&) {}
	};	// end NullLock class

	/**
	 * \brief A thread-safety definition for no thread-safety
	 */
	using NoThreadSafety =
		ThreadSafetyDefinition<NullMutex, NullLock, NullLock>;

	/**
	 * A thread-safety definition that uses standard C++ thread-safety
	 * definitions
	 */
	using StandardThreadSafety =
		ThreadSafetyDefinition<
		std::shared_mutex,
		std::shared_lock<std::shared_mutex>,
		std::unique_lock<std::shared_mutex> >;

}	// end properties namespace

#endif
