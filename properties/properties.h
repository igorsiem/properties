/*
 * This file is part of the Properties library, written by Igor Siemienowicz
 * and maintained at https://bitbucket.org/igorsiem/properties
 */

/**
 * \file properties.h Main header for the Properties library; includes all
 * other headers
 */

#include <functional>
#include "readable.h"
#include "writeable.h"
#include "readwrite.h"
#include "container.h"

/**
* \copyright Copyright (c) 2016 Igor Siemienowicz All rights reserved
* Distributed under the Boost Software License, Version 1.0. (See
* accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/
