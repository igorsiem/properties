/*
* This file is part of the Properties library, written by Igor Siemienowicz
* and maintained at https://bitbucket.org/igorsiem/properties
*/

/**
* \file writeable.h WriteableProperty class template declarations
*/

#ifndef _PROPERTIES_writeable_h_included
#define _PROPERTIES_writeable_h_included

namespace properties
{
	/**
	* \brief An interface for a property whose value can be written to with
	* a 'setter' function / lambda, which is supplied to the constructor
	*
	* This setter should not be set to `nullptr` (value update behaviour
	* would be undefined).
	*
	* When copying objects containing Properties, consider the information
	* provided on the \ref copying-properties page.
	*
	* \tparam DataType the type of the data that this property encapsulates
	*/
	template <typename DataType>
	class WriteableProperty
	{

	public:

		/**
		* \brief Definition of the internal data types
		*/
		using writeable_data_type = DataType;

		/**
		* \brief A function / lambda that sets the data
		*/
		using setter = std::function<void(const writeable_data_type&)>;

		/**
		* \brief Initialise the writeable property object with an initial
		* value and a setter function
		*
		* Note that this writeable interface does not take an initial value,
		* because it does not directly manage storage.
		*/
		explicit WriteableProperty(setter fn) : setter_fn(fn) {}

		/**
		* \brief Set the value of the Writeable Property
		*/
		virtual void set(const writeable_data_type& v) { setter_fn(v); }

		/**
		* \brief Assign to the WriteableProperty by value
		*/
		WriteableProperty<DataType>& operator=(const writeable_data_type& v)
		{
			set(v); return *this;
		}

	protected:

		/**
		* \brief The setter function / lambda
		*/
		setter setter_fn;

	};	// end WriteableProperty template class

}	// end properties namespace

#endif
