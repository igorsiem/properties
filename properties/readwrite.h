/*
* This file is part of the Properties library, written by Igor Siemienowicz
* and maintained at https://bitbucket.org/igorsiem/properties
*/

/**
* \file readwrite.h Read/write Property class declarations
*/

#ifndef _PROPERTIES_readwrite_h_included
#define _PROPERTIES_readwrite_h_included

namespace properties
{
	
	/**
	* \brief A Property that is both readable and writeable
	*
	* This class template is derived from both WriteableProperty and one of
	* the Readable Property classes (depending on a template argument).
	*
	* When copying objects containing Properties, consider the information
	* provided on the \ref copying-properties page.
	*
	* \tparam DataType the type of the data encapsulated by this property
	*
	* \tparam ReadableInterfaceType The type of Readable Property (reference
	* or value) from which the ReadWriteProperty will also be derived; this
	* should be set to either `ReadableReferenceProperty<DataType>` (the
	* default) or `ReadableValueProperty<DataType>`
	*/
	template <
		typename DataType,
		typename ReadableInterfaceType =
			ReadableReferenceProperty<DataType> >
	class ReadWriteProperty :
		public ReadableInterfaceType,
		public WriteableProperty<DataType>
	{

	public:

		// -- Public sub-types --

		/**
		* \brief The data type, defined here so that it can be used
		* externally
		*/
		using data_type = DataType;

		/**
		* \brief The 'getter' function / lambda type, redefined here from
		* the Readable Property base class for convenience
		*
		* Note that this needs to return either by const reference or by
		* value, depending on which readable interface was specified.
		*/
		using getter = typename ReadableInterfaceType::getter;

		/**
		* \brief The 'setter' function / lambda type, redefined here from
		* the WriteableProperty base class for convenience
		*/
		using setter = typename WriteableProperty<DataType>::setter;

		// -- Methods --

		/**
		* \brief Constructor, setting up both the getter and the setter
		*/
		ReadWriteProperty(getter get_fn, setter set_fn) :
			ReadableInterfaceType(get_fn),
			WriteableProperty<DataType>(set_fn)
		{}

		/**
		* \brief Assign to the Property by value
		*/
		ReadWriteProperty<DataType, ReadableInterfaceType>&
		operator=(
			const writeable_data_type& v)
		{
			set(v); return *this;
		}

	};	// end ReadWriteProperty

	/**
	 * \brief An alias for the ReadWriteProperty with the read-by-reference
	 * interface variant.
	 *
	 * \tparam DataType the type of the data that this property encapsulates
	 */
	template <typename DataType>
	using ReadReferenceWriteProperty = ReadWriteProperty<DataType>;

	/**
	 * \brief An alias for the ReadWriteProperty with the read-by-value
	 * interface variant
	 *
	 * \tparam DataType the type of the data that this property encapsulates
	 */
	template <typename DataType>
	using ReadValueWriteProperty =
		ReadWriteProperty<DataType, ReadableValueProperty<DataType> >;

}	// end properties namespace

#endif
