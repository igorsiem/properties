/*
* This file is part of the Properties library, written by Igor Siemienowicz
* and maintained at https://bitbucket.org/igorsiem/properties
*/

/**
* \file readable.h Readable property class template declarations
*/

#ifndef _PROPERTIES_readable_h_included
#define _PROPERTIES_readable_h_included

/**
* \brief Contains all declarations in the Properties library
*/
namespace properties
{

	/**
	 * \brief A property that can retrieve data by value, using a 'getter'
	 *
	 * The getter is a function / lambda that is supplied on construction,
	 * and returns the Property data by value. It should not be set to
	 * `nullptr` (retrieval behaviour will be undefined).
	 *
	 * When copying objects containing Properties, consider the information
	 * provided on the \ref copying-properties page.
	 *
	 * \tparam DataType The type of the data that this property encapsulates
	 */
	template <typename DataType>
	class ReadableValueProperty
	{
	public:

		/**
		* \brief Define the internal data type
		*/
		using readable_data_type = DataType;

		/**
		* \brief A function / lambda that returns the value type
		*/
		using getter = std::function<const readable_data_type(void)>;

		/**
		* \brief Construct the readable property object with the getter
		* function
		*/
		explicit ReadableValueProperty(getter fn) : getter_fn(fn) {}

		// -- Retrieval --

		/**
		* \brief Retrieve the value
		*/
		virtual readable_data_type get(void) const { return getter_fn(); }

		/**
		* \brief Retrieve using the function-call operator
		*/
		readable_data_type operator()(void) const { return get(); }

		/**
		* \brief Retrieve using cast-to-value, so that properties can be
		* used in place of their value.
		*/
		operator readable_data_type(void) const { return get(); }

		// -- Comparison --

		// Note that we deliberately don't rely on std::rel_ops here because
		// they don't work properly with std::string and char*

		/**
		 * \brief Equality comparison
		 */
		bool operator==(const readable_data_type& v) const
		{
			return (get() == v);
		}

		/**
		 * \brief Not-equal comparison
		 */
		bool operator!=(const readable_data_type& v) const
		{
			return !operator==(v);
		}

		/**
		 * \brief Less-than comparison
		 */
		bool operator<(const readable_data_type& v) const
		{
			return (get() < v);
		}

		/**
		 * \brief Less-than-or-equal comparison
		 */
		bool operator<=(const readable_data_type& v) const
		{
			return (operator<(v) || operator==(v));
		}

		/**
		 * \brief Greater-than operator
		 */
		bool operator>(const readable_data_type& v) const
		{
			return !operator<=(v);
		}

		/**
		 * \brief Greater-than-or-equal comparison
		 */
		bool operator>=(const readable_data_type& v) const
		{
			return !operator<(v);
		}

	protected:

		/**
		 * \brief The 'getter' function / lambda object that is used to
		 * retrieve the data
		 */
		getter getter_fn;

	};	// end ReadableValueProperty template class

	/**
	 * \brief A property that can retrieve data by reference, using a
	 * 'getter'
	 *
	 * The getter is a function / lambda that is supplied on construction,
	 * returning the Property data by reference. It should not be set to
	 * `nullptr` (retrieval behaviour will be undefined).
	 *
	 * When copying objects containing Properties, consider the information
	 * provided on the \ref copying-properties page.
	 *
	 * \tparam DataType the type of the data that this property encapsulates
	 */
	template <typename DataType>
	class ReadableReferenceProperty
	{
	public:

		/**
		 * \brief Define the internal data type
		 */
		using readable_data_type = DataType;

		/**
		 * \brief A function / lambda that returns the value type
		 */
		using getter = std::function<const readable_data_type&(void)>;

		/**
		 * \brief Construct the readable property object with the getter
		 * function
		 */
		explicit ReadableReferenceProperty(getter fn) : getter_fn(fn) {}

		// -- Retrieval --

		/**
		 * \brief Retrieve the value
		 */
		virtual const readable_data_type& get(void) const
		{
			return getter_fn();
		}

		/**
		 * \brief Retrieve using the function-call operator
		 */
		const readable_data_type& operator()(void) const { return get(); }


		/**
		 * \brief Retrieve using cast-to-value, so that properties can be
		 * used in place of their value.
		 */
		operator const readable_data_type&(void) const { return get(); }

		// -- Comparison --

		// Note that we deliberately don't rely on std::rel_ops here because
		// they don't work properly with std::string and char*

		/**
		 * \brief Equality comparison
		 */
		bool operator==(const readable_data_type& v) const
		{
			return (get() == v);
		}

		/**
		 * \brief Not-equal comparison
		 */
		bool operator!=(const readable_data_type& v) const
		{
			return !operator==(v);
		}

		/**
		 * \brief Less-than comparison
		 */
		bool operator<(const readable_data_type& v) const
		{
			return (get() < v);
		}

		/**
		 * \brief Less-than-or-equal comparison
		 */
		bool operator<=(const readable_data_type& v) const
		{
			return (operator<(v) || operator==(v));
		}

		/**
		 * \brief Greater-than operator
		 */
		bool operator>(const readable_data_type& v) const
		{
			return !operator<=(v);
		}

		/**
		 * \brief Greater-than-or-equal comparison
		 */
		bool operator>=(const readable_data_type& v) const
		{
			return !operator<(v);
		}

	protected:

		/**
		 * \brief The getter function / lambda
		 */
		getter getter_fn;

	};	// end ReadableReferenceProperty template class

}	// end properties namespace

/**
 * \page copying-properties Copying Properties
 *
 * Care must be taken when copying Properties. Getter and setter function
 * objects themselves are copied (along with any bindings that they may
 * have (in the case of lambdas). In the normal usage case, where Properties
 * are attributes of objects that are being used wrap other raw data
 * attributes of the object, this is probably undesirable.
 *
 * In this situation, it is a good idea to either disable copy semantics for
 * classes using Property attributes, or 'hand-code' the copy semantics for
 * such classes so that Property objects are not copied (and correctly retain
 * bindings to their local attributes).
 */
#endif