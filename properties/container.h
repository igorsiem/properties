/*
* This file is part of the Properties library, written by Igor Siemienowicz
* and maintained at https://bitbucket.org/igorsiem/properties
*/

/**
* \file container.h ContainerProperty class template declaration
*/

#include <functional>
#include <vector>
#include "threadsafety.h"

#ifndef _PROPERTIES_container_h_included
#define _PROPERTIES_container_h_included

namespace properties
{

	/**
	 * \brief A self-contained data property supporting thread-safety,
	 * validation and notification
	 *
	 * This is a 'convenience' class that includes functionality for a number
	 * of common property usages. It declares its own internal instance of
	 * the data type, implements full thread-safety (if an appropriate
	 * thread-safety definition struct is used), and can call validation
	 * and notification functions on update.
	 *
	 * Validation prior to Property changes, and notification after changes
	 * are implemented using function / lambda objects. There may be zero or
	 * more such validator or notifier functions. They are held in vectors
	 * (which may be freely updated at any time), and are called (in vector
	 * order) before and / or after updating the Property data.
	 *
	 * Validator functions are called to check a proposed change to data.
	 * They take two arguments - the current (old) value of the Property,
	 * and the proposed new value. They return `true` to allow the change to
	 * take place. If they return `false`, the change will be disallowed, and
	 * (in the case where there are multiple validator functions) subsequent
	 * validators as well as any notifiers will *not* be called. Validators
	 * may also throw an exception to abort an update as well as signalling a
	 * problem.
	 * 
	 * Notifier functions have a simple `void(void)` definition. The code of
	 * a validator function may freely query the updated Property value.
	 *
	 * By default, this class is *not* threadsafe. It can be made thread-safe
	 * by instantiating the template with a `ThreadSafety` parameter of
	 * `StandardThreadSafety` (to use standard C++ thread-safety constructs),
	 * or implementing some other thread-safety definition struct. See the
	 * \ref thread-safety page for notes on how these definitions are
	 * implemented. If this is done, then *all* operations on the Property
	 * are thread-safe, including updating validators and notifiers.
	 *
	 * This class has full copy semantics, but note that *validators and
	 * notifers are not copied.* This is to avoid issues with validator and
	 * notifier function objects being bound to local variables. If it is
	 * safe, validator and/or notifier lists can be explicitly copied using
	 * the appropriate get / set methods.
	 *
	 * This means that - in the copy-constructor case, the destination
	 * Property will have no validators or notifiers. In the case of
	 * assignment, a standard 'set' operation for the data value is invoked.
	 * This means that the existing validators and notifiers of the
	 * destination Property are called, and the copy may possibly be
	 * disallowed by a validator.
	 *
	 * \tparam DataType The type used for data in the Property; an instance
	 * of this type is instantiated in the Property
	 *
	 * \tparam ThreadSafety The thread-safety definition structure to use
	 * see \ref thread-safety for more information
	 */
	template <typename DataType, typename ThreadSafety = NoThreadSafety>
	class ContainerProperty : public ReadReferenceWriteProperty<DataType>
	{

	public:

		// -- Public sub-types --

		/**
		 * \brief Alias for the data type
		 */
		using data_type = DataType;

		/**
		 * \brief Alias for the thread-safety definition structure
		 */
		using thread_safety = ThreadSafety;

		/**
		 * \brief Alias for the mutex type used for thread-safety
		 */
		using mutex = typename thread_safety::shared_mutex;

		/**
		 * \briuef Alias for the lock type used for reading
		 */
		using read_lock = typename thread_safety::shared_lock;

		/**
		 * \brief Alias for the lock type used for writing
		 */
		using write_lock = typename thread_safety::unique_lock;

		/**
		 * \brief Definition for a function / lamda that validates a
		 * Property value change
		 *
		 * Its arguments are the old value and the proposed new value, and it
		 * returns `true` if the change should be permitted. Validators may
		 * also throw an exception to disallow the change *and* signal a
		 * problem.
		 */
		using validator =
			std::function<bool(const data_type&, const data_type&)>;

		/**
		 * \brief A vector of validator functions
		 */
		using validator_vector = std::vector<validator>;

		/**
		 * \brief Definition for a function / lamda that notifies that a
		 * Property's value has been updated
		 *
		 * Note that the body of a notifier may freely query the value of
		 * its Property (with appropriate bindings).
		 */
		using notifier = std::function<void(void)>;

		/**
		 * \brief A vector of notifier functions
		 */
		using notifier_vector = std::vector<notifier>;

		// -- Public methods --

		/**
		 * \brief Sets up the ContainerProperty with an initial value, and
		 * optional vectors of validators and notifiers
		 *
		 * Note that this constructor sets up the getter and setter to
		 * perform all appropriate thread-safety, validation and notification
		 * functionality.
		 *
		 * \param data Initial value for the Property data
		 *
		 * \validators An optional initialiser list or vector of validators;
		 * these will be called in the order specified
		 *
		 * \notifiers An optional initialiser list or vector of notifiers;
		 * these will be called in the order specified
		 */
		ContainerProperty(
				const data_type& data,
				const validator_vector& validators = {},
				const notifier_vector& notifiers = {}) :
			ReadReferenceWriteProperty<DataType>(
				[this](void)->const data_type&{ return get_impl(); },
				[this](const data_type& v) { set_impl(v); }),
			m_data_mutex(),
			m_data(data),
			m_val_not_mutex(),
			m_validators(validators),
			m_notifiers(notifiers)
		{}

		/**
		 * \brief Copy constructor
		 *
		 * Note that *data is copied, but validators and notifiers are not*.
		 * The newly constructed Property will have no validators or
		 * notifiers.
		 */
		ContainerProperty(
				const ContainerProperty<DataType, ThreadSafety>& rhs) :
			ReadReferenceWriteProperty<DataType>(
				[this](void)->const data_type&{ return get_impl(); },
				[this](const data_type& v) { set_impl(v); }),
			m_data_mutex(),
			m_data(rhs.m_data),
			m_val_not_mutex(),
			m_validators(),
			m_notifiers()
		{}

		/**
		 * \brief Assignment operator
		 *
		 * This method simply calls 'set', to copy the data. *Validators and
		 * notifiers are not copied* from the source Property, *but* they are
		 * called, which means that the copy may be disallowed if a validator
		 * returns false.
		 */
		ContainerProperty<DataType, ThreadSafety>& operator=(
			const ContainerProperty<DataType, ThreadSafety>& rhs)
		{
			set(rhs.get());
			return *this;
		}

		/**
		* \brief Assign to the Property by value
		*/
		ContainerProperty<DataType, ThreadSafety>& operator=(
				const data_type& v)
		{
			set(v); return *this;
		}

		/**
		 * \brief Retrieve a copy of the vector of validators
		 */
		const validator_vector& get_validators(void) const
		{
			read_lock l(m_val_not_mutex);
			return m_validators;
		}

		/**
		 * \brief Set the vector of validators
		 */
		void set_validators(const validator_vector& validators)
		{
			write_lock l(m_val_not_mutex);
			m_validators = validators;
		}

		/**
		 * \brief Add a new validator to the end of the vector of validators
		 */
		void add_validator(validator v)
		{
			write_lock l(m_val_not_mutex);
			m_validators.push_back(v);
		}

		/**
		 * \brief Retrieve the vector of notifiers
		 */
		const notifier_vector& get_notifiers(void) const
		{
			read_lock l(m_val_not_mutex);
			return m_notifiers;
		}

		/**
		 * \brief Set the vector of notifiers
		 */
		void set_notifiers(const notifier_vector& notifiers)
		{
			write_lock l(m_val_not_mutex);
			m_notifiers = notifiers;
		}

		/**
		 * \brief Add a notifier to the end of the vector of notifiers
		 */
		void add_notifier(notifier n)
		{
			write_lock l(m_val_not_mutex);
			m_notifiers.push_back(n);
		}

	protected:

		// -- Internal Helper Functions --

		/**
		 * \brief Implements the getter, with proper thread-safety
		 */
		const data_type& get_impl(void) const
		{
			read_lock l(m_data_mutex);
			return m_data;
		}	// end get_impl

		/**
		 * \brief Implements the setter, with thread-safety, validation and
		 * notification
		 */
		void set_impl(const data_type new_value)
		{
			// Call the validators first.
			{
				read_lock val_lock(m_val_not_mutex);
				read_lock data_lock(m_data_mutex);
				for (auto validator : m_validators)
				{
					if (!validator(m_data, new_value)) return;
				}
			}

			// Update the data
			{
				write_lock l(m_data_mutex);
				m_data = new_value;
			}

			// Now call the notifiers
			read_lock not_lock(m_val_not_mutex);
			for (auto notifier : m_notifiers) notifier();

		}	// end set_impl

		// -- Internal Attributes --

		/**
		 * \brief The mutex that protects the Property data
		 */
		mutable mutex m_data_mutex;

		/**
		 * \brief The Property data
		 */
		data_type m_data;

		/**
		 * \brief Mutex that protects the validator and notifier vectors
		 */
		mutable mutex m_val_not_mutex;

		/**
		 * \brief The vector of validator functions
		 */
		validator_vector m_validators;

		/**
		 * \brief The vector of notifier functions
		 */
		notifier_vector m_notifiers;

	};	// end ContainerProperty class

}	// end properties namespace

#endif
